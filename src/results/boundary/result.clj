(ns results.boundary.result
  (:require [duct.database.mongodb.monger]
            [monger.collection :as mc]
            [monger.query :as mq]
            [monger.conversion :refer [from-db-object]]))


(defprotocol Result
  (fetch-all [db user-id])
  (fetch-last-n [db user-id n])
  (fetch-one-from-last [db user-id i]))


(extend-protocol Result
  duct.database.mongodb.monger.Boundary

  (fetch-all [{:keys [db]} user-id]
    (let [coll "result"
          user-id (Integer/parseInt user-id)]
      (mq/with-collection db coll
        (mq/find {:userId user-id})
        (mq/fields [:userId :startTime :endTime :data])
        (mq/sort {:startTime 1}))))

  (fetch-last-n [{:keys [db]} user-id n]
    (let [coll "result"
          user-id (Integer/parseInt user-id)
          cnt (Integer/parseInt n)]
      (mq/with-collection db coll
        (mq/find {:userId user-id})
        (mq/sort {:startTime -1})
        (mq/limit cnt)
        (mq/fields [:userId :startTime :endTime :data]))))

  (fetch-one-from-last [{:keys [db]} user-id i]
    (let [coll "result"
          user-id (Integer/parseInt user-id)
          index (Integer/parseInt i)]
      (mq/with-collection db coll
        (mq/find {:userId user-id})
        (mq/sort {:startTime -1})
        (mq/skip index)
        (mq/limit 1)
        (mq/fields [:userId :startTime :endTime :data])))))
