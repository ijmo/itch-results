(ns results.handler.v1
  (:require [ataraxy.core :as ataraxy]
            [ataraxy.response :as response]
            [integrant.core :as ig]
            [results.boundary.result :refer [fetch-all fetch-last-n fetch-one-from-last]]
            [duct.logger :as logger]))


(defmethod ig/init-key ::get-all-data [_ {:keys [db logger]}]
  (fn [{[_ user-id] :ataraxy/result}]
    (if-let [res (fetch-all db user-id)]
      (do (logger/log logger :info ::all-data {:user-id user-id
                                               :result-count (count res)})
          [::response/ok {:data (map #(select-keys % [:userId :startTime :endTime :data]) res)}])
      [::response/ok "ok"])))


(defmethod ig/init-key ::get-last [_ {:keys [db logger]}]
  (fn [{[_ user-id n] :ataraxy/result}]
    (if-let [res (fetch-last-n db user-id n)]
      (do (logger/log logger :info ::get-last {:user-id user-id
                                               :count n
                                               :result-count (count res)})
          [::response/ok {:data (map #(select-keys % [:userId :startTime :endTime :data]) (reverse res))}])
      [::response/ok "ok"])))


(defmethod ig/init-key ::get-reversed [_ {:keys [db logger]}]
  (fn [{[_ user-id i] :ataraxy/result}]
    (if-let [res (fetch-one-from-last db user-id i)]
      (do (logger/log logger :info ::reversed {:user-id user-id
                                               :index i
                                               :result-count (count res)})
          [::response/ok {:data (map #(select-keys % [:userId :startTime :endTime :data]) res)}])
      [::response/ok "ok"])))
