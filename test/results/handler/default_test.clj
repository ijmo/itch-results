(ns results.handler.default-test
  (:require [clojure.test :refer :all]
            [integrant.core :as ig]
            [ring.mock.request :as mock]
            [results.handler.default :as default]))

(deftest smoke-test
  (testing "example page exists"
    (let [handler  (ig/init-key :results.handler.default/health {})
          response (handler (mock/request :get "/health"))]
      (is (= :ataraxy.response/ok (first response)) {:status "UP"}))))
